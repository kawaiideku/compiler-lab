%{
	#include <stdio.h>
	int yylex();
	void yyerror(char const *);
%}

%token NUM
%left '+' '-'
%left '/' '*'
%%

input	: %empty
	| input line
	;
line	: '\n'
	| exp '\n'	{ printf ("= %d\n", $1); }
	;
exp	: exp '+' exp 	{ $$ = $1 + $3; }
	| exp '-' exp 	{ $$ = $1 - $3; }
	| exp '*' exp 	{ $$ = $1 * $3; }
	| exp '/' exp 	{ $$ = $1 / $3; }
	| NUM		{ $$ = $1;	}
	;

%%
void
main()
{
	yyparse();
}
