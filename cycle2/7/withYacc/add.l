%{
	#include <stdio.h>
	#include <string.h>
	#include "add.tab.h"
%}

%%

[0-9]+	{
		yylval = atoi (yytext);
		/* printf ("%s", yytext); */
		return NUM;
	}
[\t]	{ /*skip*/ }
" "	{ /*Investigate why rules in previous commit doesn't work*/ }
[\n]|.	{ return yytext[0]; }

%%
void
yyerror(char const *s)
{
	fprintf (stderr, "%s\n", s);
}
int
yywrap()
{
	return 1;
}
