#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LEXEME_LEN 20

//globals
char* symbols[]   = {"{", "}", "(", ")", ";"};
char* operators[] = {">", "<", "=", "==", ">=", "<=", "+", "-", "*", "/"};
char* keywords[]  = {"main", "int", "for", "if", "printf", "scanf", "return"};

int in_str_list(char* word, char* list[])
{
	int i = 0;
	while (list[i]) {
		if (!strcmp (list[i], word))
			return 1;
		i++;
	}
	return 0;
}
int is_keyword(char* lexeme)
{
	return in_str_list(lexeme, keywords);
}
int is_operator(char* lexeme)
{
	return in_str_list(lexeme, operators);
}
int is_constant(char* lexeme)
{
	int i, is_digit;
	//number constant
	for (i = 0; lexeme[i] != '\0'; ++i) {
		is_digit = 1;
		if (!isdigit ((int) lexeme[i])) {
			is_digit = 0;
			break;
		}
	}
	return is_digit;
}
int is_symbol(char* lexeme)
{
	return in_str_list(lexeme, symbols);
}
int is_identifier(char* lexeme)
{
	if (lexeme[0] == '_')   return 1;
	if ispunct((lexeme[0])) return 0;
	if isdigit((lexeme[0]))	return 0;

	return 1;
}
int main(int argc, char *argv[])
{
	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		printf("File not found!\n");
		return 1;
	}
	char* lexeme = malloc(sizeof(char) * LEXEME_LEN);
	while (fscanf (fp, "%s", lexeme) != EOF) {
		if (is_keyword (lexeme))
			printf ("%s \t Keyword\n",  lexeme);
		else if (is_operator (lexeme))
			printf ("%s \t Operator\n", lexeme);
		else if (is_constant (lexeme))
			printf ("%s \t Constant\n", lexeme);
		else if (is_symbol (lexeme))
			printf ("%s \t Symbol\n",   lexeme);
		else if (is_identifier (lexeme))
			printf("%s \t Identifier\n", lexeme);
		else
			printf("%s \t Unknown lexeme\n", lexeme);
	}
	return 0;
}
/* vim:set nospell: */
