#!/usr/bin/env bash

# indent all c, yacc and lex files
for i in `find . -name "*.c"` 
do
	cp $i $i.md
	sed -i s/^/"\t"/ $i.md
done

# generate md files for image
for i in `find . -name "*.png"`
do
	echo $i
	echo "![]($i)" > $i.md
done


