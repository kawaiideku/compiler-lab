#!/usr/bin/env bash
for i in `find . -name "*.l" && find . -name "*.y"` 
do
	sed -i s/^/"\t"/ $i
	mv $i $i.md
done
