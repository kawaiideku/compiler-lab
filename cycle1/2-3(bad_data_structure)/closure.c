#include "closure.h"

#include <stdio.h>
#include <stdlib.h>

//globals
char* nfa[NFA_LEN][NFA_LEN];
int   num_states, num_symbols;
char  symbols[SYMS_LEN];

/* int main() */
/* { */
/* 	int i, j; */
/* 	read_nfa_with_E(); */
/* 	find_E_closures(); */

/* 	printf ("\n\nThe E closures are :\n"); */
	
/* 	for (i = 0; i < num_states; ++i) { */
/* 		printf ("\n%d :\t", i); */
/* 		for (j = 0; j < states[i].E_closure_len; ++j) */
/* 			printf ("%d, ", states[i].E_closure[j]); */
/* 		printf ("\b\b \n"); */

/* 	} */
/* 	printf ("\n"); */
/* 	return 0; */
/* } */

/* E stands for epsilon throughout the file */
void read_nfa_with_E()
{
	int i, j;

	printf("Enter no. of states : ");
	scanf ("%d", &num_states);

	for (i = 0; i < num_states; ++i) {
		states[i].E_closure[0]  = i;
		states[i].E_closure_len = 1;
	}
	printf ("Enter no. of input symbols : ");
	scanf ("%d", &num_symbols);
	printf ("Enter input symbols : ");
	for (i = 0; i < num_symbols; ++i)
		scanf (" %c", &symbols[i]);

	//add E as last symbol
	symbols[num_symbols] = 'E';

	printf ("\nEnter the NFA as matrix (- for blank) :\n\t");
	for (i = 0; i < num_symbols + 1; ++i)
		printf ("%c\t", symbols[i]);
	printf ("\n");

	for (i = 0; i < num_states; ++i) {
		printf ("%d\t", i);
		//add extra column for E symbol
		for (j = 0; j < num_symbols + 1; ++j) {
			nfa[i][j] = malloc (sizeof(char) * STR_LEN);
			scanf ("%s", nfa[i][j]);
		}
	}
}

void find_E_closures()
{
	int i;
	for (i = 0; i < num_states; ++i)
		calc_E_closure (&states[i]);
}
void calc_E_closure(struct state_t* state)
{
	int i, E_col = num_symbols;
	for (i = 0; i < state->E_closure_len; ++i) {
		char* E_col_text = nfa[state->E_closure[i]][E_col];
		E_closure_union (state, E_col_text);
	}
}
void E_closure_union(struct state_t* state, char* E_col_text)
{
	//takes letter as state number, so only works for 1 digit states
	int i, dest_state_id;
	for (i = 0; E_col_text[i] != '\0'; ++i) {
		if (E_col_text[i] == '-')
			continue;
		dest_state_id = E_col_text[i] - '0';
		if (!in_int_list (dest_state_id, state->E_closure, state->E_closure_len))
			state->E_closure[state->E_closure_len++] = dest_state_id;
	}
}
int in_int_list(int number, int list[], int n)
{
	int i;
	for (i = 0; i < n; ++i)
		if (number == list[i])
			return 1;
	return 0;
}
/* vim:set nospell:*/

