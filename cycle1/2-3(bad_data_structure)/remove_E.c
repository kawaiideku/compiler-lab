/***********************************
*  remove E transitions from nfa  *
***********************************/
#include "closure.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

//globals
char* nfa_without_E[NFA_LEN][NFA_LEN];
char* new_states[NFA_LEN];
int   new_nfa_row_ctr;
void add_to_nfa_without_E (int dest_index,
			   int new_nfa_row,
			   int col)
{
	if (!nfa_without_E[new_nfa_row][col]) {
		nfa_without_E[new_nfa_row][col] = malloc (sizeof(char) *
							      STR_LEN);
	}
	char* c = malloc (sizeof(char) * STR_LEN);
	if (!strchr (nfa_without_E[new_nfa_row][col],
				dest_index - '0')) {

		/* TODO: investigate why using malloc works <23-10-19, Balamurali M> */
		sprintf (c, "%d", dest_index);
		strcat  (nfa_without_E[new_nfa_row][col], c);
	}
}

void check_E_closure(char* dest, int col)
{
	//col: to be passed to add_to_nfa_without_E
	int i, k, *E_closure, E_closure_len;
	for (i = 0; i < num_states; ++i) {
		E_closure     = states[i].E_closure;
		E_closure_len = states[i].E_closure_len;
		//check all dests in dest list
		for (k = 0; dest[k] != '\0'; ++k) {
			if (in_int_list (dest[k] - '0',
					 E_closure,
					 E_closure_len))
				add_to_nfa_without_E (i, new_nfa_row_ctr, col);
		}
	}
}
void check_row(int elem)
{
	//check the element's row for values and sees what closures they
	//belong in
	int   col;
	char* dest;
	for (col = 0; col < num_symbols; ++col) {
		//col is symbol
		dest = nfa[elem][col];
		check_E_closure (dest, col);
	}
}

void build_new_nfa()
{
	int elem, i, j, *E_closure, E_closure_len;

	//closure is new state
	for (i = 0; i < num_states; ++i) {
		new_nfa_row_ctr = i;
		E_closure       = states[i].E_closure;
		E_closure_len   = states[i].E_closure_len;
		//check row of each element of closure
		for (j = 0; j < E_closure_len; ++j) {
			elem = E_closure[j];
			check_row (elem);
		}
	}
}
void display_new_nfa ()
{
	int i, j;
	for (i = 0; i < num_symbols; ++i)
		printf ("	%c", symbols[i]);
	printf ("\n");
	for (i = 0; i < num_states; ++i) {
		printf ("%d	", i);
		for (j = 0; j < num_symbols; ++j) {
			if (nfa_without_E[i][j])
				printf ("%s	", nfa_without_E[i][j]);
			else
				printf ("-	");
		}
		printf ("\n");
	}
}
int main ()
{
	read_nfa_with_E ();
	printf          ("\n\nNew NFA without E is\n\n");
	find_E_closures ();
	build_new_nfa   ();
	display_new_nfa ();
}
/* vim:set noet sts=0 sw=8 ts=8 tw=79: */
