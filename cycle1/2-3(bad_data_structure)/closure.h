#ifndef CLOSURE_H
#define CLOSURE_H

#define SYMS_LEN 20
#define STR_LEN 20
#define NFA_LEN 10

//globals
extern char* nfa[NFA_LEN][NFA_LEN];
extern int   num_states, num_symbols;
extern char  symbols[SYMS_LEN];
struct state_t {
	int E_closure[NFA_LEN], E_closure_len;
} states[NFA_LEN];

extern void read_nfa_with_E ();
extern void find_E_closures ();
extern void calc_E_closure  (struct state_t* state);
extern void E_closure_union (struct state_t* state, char* E_col_text);
extern int  in_int_list     (int number, int list[], int n);

#endif
