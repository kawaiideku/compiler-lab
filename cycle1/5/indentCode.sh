#!/usr/bin/env bash
for i in `find . -name "*.l"` 
do
	sed -i s/^/"\t"/ $i
	mv $i $i.md
done
